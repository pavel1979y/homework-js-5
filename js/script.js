let user= {
    name:'Pety',
    age: 28,
    pets: {
        name:'Guga',
        type:'pig'
    }
};
function objectCloning(objectToClone) {
    let objectClon= {};

    for (let key in objectToClone){
        if (typeof objectToClone[key] !== 'object'){
            objectClon[key] = objectToClone[key];
        } else {
            objectClon[key] = objectCloning(objectToClone[key]);
        }
    }
    return objectClon
}

let userClon=objectCloning(user);

userClon.name ='Lola';
userClon.pets.name = 'Bobik';
console.log('original: ');
console.log(user);
console.log('clone: ');
console.log(userClon);